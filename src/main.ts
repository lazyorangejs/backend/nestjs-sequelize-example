// https://emojipedia.org/rocket/
require('dotenv-safe').config()

import { ShutdownSignal } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'
import * as pino from 'pino'

async function bootstrap (port: number = 3000, level: string = 'debug') {
  const logger = pino({ level })
  const app = await NestFactory.create(AppModule)
  app.useLogger(logger)
  app.enableShutdownHooks([ShutdownSignal.SIGINT, ShutdownSignal.SIGTERM])

  const options = new DocumentBuilder()
    .setTitle('Nest.js Sequealize example')
    .setVersion('1.0')
    .addTag('users')
    .build()
  const document = SwaggerModule.createDocument(app, options)
  SwaggerModule.setup('api', app, document)

  await app.listen(port)
  logger.info({ port }, 'Server has been successfully started 🚀')

  setInterval(() => {
    logger.debug(
      `The server uses approximately ${Math.round(
        process.memoryUsage().rss / (1024 * 1024)
      )} MB`
    )
  }, 5000)
}

const httpPort = parseInt(process.env.PORT, 10) || 5000
const loglevel = process.env.LOG_LEVEL || 'debug'

bootstrap(httpPort, loglevel)
