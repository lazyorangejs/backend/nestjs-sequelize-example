import { Controller, Get, OnApplicationShutdown } from '@nestjs/common'
import { AppService } from './app.service'

const logger = console

@Controller()
export class AppController implements OnApplicationShutdown {
  constructor (private readonly appService: AppService) {}

  @Get()
  getHello (): string {
    return this.appService.getHello()
  }

  onApplicationShutdown () {
    logger.log('I`ve received shutdown signal')
  }
}
