import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { DebugService } from './debug/debug.service'
import { DebugModule } from './debug/debug.module'
import { UsersModule } from './users/users.module'
import { SequelizeModule } from '@nestjs/sequelize'

import { URL, format } from 'url'
import * as pino from 'pino'

const logger = pino({ level: process.env.LOG_LEVEL || 'debug' })

@Module({
  imports: [
    DebugModule,
    UsersModule,
    SequelizeModule.forRootAsync({
      useFactory: () => {
        const parsedUrl = new URL(process.env.DATABASE_URL)

        const { hostname: host, port, username, password, pathname: db } = parsedUrl
        const sslOptions = parsedUrl.searchParams.get('sslmode') === 'require' ? {
          ssl: true,
          // Ref.: https://github.com/brianc/node-postgres/issues/2009
          rejectUnauthorized: !parsedUrl.host.includes('ondigitalocean.com')
        } : false

        parsedUrl.password = '*'.repeat(parsedUrl.password.length)
        logger.info(`Connected to ${format(parsedUrl)}`)

        return {
          dialect: 'postgres',
          dialectOptions: {
            ssl: sslOptions,
            keepAlive: true
          },
          host,
          port: parseInt(port, 10),
          username,
          password,
          database: db.substring(1),
          autoLoadModels: true,
          synchronize: true
        }
      }
    })
  ],
  controllers: [AppController],
  providers: [AppService, DebugService]
})
export class AppModule {}
