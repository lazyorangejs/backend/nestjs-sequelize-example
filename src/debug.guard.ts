import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common'
import { Observable } from 'rxjs'

@Injectable()
export class DebugGuard implements CanActivate {
  canActivate (
    _: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {
    if (process.env.NODE_ENV === 'development') {
      return true
    }
    return false
  }
}
