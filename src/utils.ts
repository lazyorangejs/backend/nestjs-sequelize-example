import * as util from 'util'

export const denyAccessToEnv = (logger = console) => {
  const p = new Proxy(process.env, {
    get (env, property: string) {
      if (util.isString(property)) {
        logger.debug(
          { property },
          `Someone tries to read a property from process.env, will be returned an empty string.
            Please, use another way to deal with configuration (https://stackoverflow.com/a/57317215)`
        )
        return ''
      }
      return env[property]
    }
  })

  process.env = p
}
